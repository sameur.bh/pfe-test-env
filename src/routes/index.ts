import { Router } from "express"
import gpt from "./gpt.route"

const root = Router()

root.use('/gpt', gpt)

export default root