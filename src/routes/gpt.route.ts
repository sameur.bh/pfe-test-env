import { Router } from "express"
import * as controllers from "../controllers/gpt.controller"

const route = Router()

route.post('/', controllers.askGPT)
route.post('/json', controllers.askJsonGPT)

export default route