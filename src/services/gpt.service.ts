import OpenAI from 'openai'
import { APIPromise } from 'openai/core'
import dotenv from 'dotenv'

dotenv.configDotenv()

const openai = new OpenAI({
    apiKey: process.env.OPENAI_API_KEY
})

export const getGPTResponse = (message: string): APIPromise<OpenAI.Chat.Completions.ChatCompletion> => {
    return openai.chat.completions.create({
        messages: [{ role: "system", content: message }],
        model: "gpt-4",
    })
}