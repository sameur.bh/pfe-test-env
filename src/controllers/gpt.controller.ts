import { Request, Response } from 'express'
import * as services from '../services/gpt.service'

export const askGPT = (req: Request, res: Response) => {
    const { message } = req.body
    services.getGPTResponse(message).then((response) => {
        res.send(response.choices[0].message.content)
    }).catch((error) => {
        res.send(error)
    })
}

export const askJsonGPT = (req: Request, res: Response) => {
    const { message } = req.body
    services.getGPTResponse(message).then((response) => {
      res.json(response.choices[0].message.content)
    }).catch((error) => {
      res.json(error)
    })
  }