import express from 'express'
import { json } from 'body-parser'
import routes from './routes'

const app = express()
const port = process.env.PORT

app.use(json())

app.use(routes)

app.listen(port, () => {
  console.log(`🚀 server started at http://localhost:${port}`)
})